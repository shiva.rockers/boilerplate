/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-continue */
const fs = require('fs');
const path = require('path');

const { Location } = require('./../../models');

function Operations() {
    const locations = fs.readFileSync(path.join(__dirname, 'dir', 'db_locations.json'));
    this.locations = {};
    try {
        this.locations = JSON.parse(locations);
    } catch (e) {
        log.console(e);
    }
}

Operations.prototype.dump = async function() {
    const counter = 0;
    const invalidData = new Set();
    const locationsArray = [];

    for (const key in this.locations) {
        const locationObj = {};

        locationObj.key = key;
        locationObj.locations = [];

        if (!this.locations[key]) continue;

        if (this.locations[key].code) locationObj.code = this.locations[key].code;
        else invalidData.add(key);

        if (this.locations[key].href) locationObj.href = this.locations[key].href;
        else invalidData.add(key);

        if (!this.locations[key].locations) {
            invalidData.add(key);
            continue;
        }
        for (const [_key, _val] of Object.entries(this.locations[key].locations)) locationObj.locations.push({ key: _key, val: _val });

        // await Location.create(locationObj);
        // console.log(counter++, 'done');
        locationsArray.push(locationObj);
    }
};

module.exports = new Operations();
