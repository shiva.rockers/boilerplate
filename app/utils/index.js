const locationProcess = require('./lib/locationProcess');
const mongodb = require('./lib/mongodb');

module.exports = {
    locationProcess,
    mongodb,
};
