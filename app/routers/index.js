const http = require('http');
const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const compression = require('compression');
const morgan = require('morgan');

/** Routes */

// const userRoutes = require('./routes');

function Router() {
    this.app = express();
    this.httpServer = http.createServer(this.app);
    this.corsOptions = {
        origin: ['*'],
        methods: ['GET', 'POST'],
        AllowedOrigin: ['*'],
        allowedHeaders: ['Content-Type', 'authorization', 'verification'],
        exposedHeaders: ['authorization', 'verification'],
        optionsSuccessStatus: 200,
    };
}

Router.prototype.initialize = function() {
    this.setupMiddleware();
    this.setupServer();
};

Router.prototype.setupMiddleware = function() {
    this.app.disable('etag');
    this.app.enable('trust proxy');

    this.app.use(helmet({}));
    this.app.use(cors());
    this.app.use(compression());
    this.app.use(express.json({}));
    this.app.use(express.urlencoded({}));
    this.app.use(express.static('./seeds'));

    if (process.env.NODE_ENV !== 'prod') this.app.use(morgan('dev', { skip: req => req.path === '/ping' || req.path === '/favicon.ico' }));

    this.app.use(this.routeConfig);

    /** Router imports start */
    // this.app.use('/api/v1', routes);
    /** Router imports ends */

    this.app.use('*', express.static('./seeds'));
    this.app.use(this.logErrors);
    this.app.use(this.errorHandler);
};

Router.prototype.setupServer = function() {
    this.httpServer.timeout = 20000;
    this.httpServer.listen(process.env.PORT, '0.0.0.0', () => log.green(`Spinning on ${process.env.PORT}`));
};

Router.prototype.routeConfig = function(req, res, next) {
    req.sRemoteAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Expose-Headers', 'authorization, verification');

    if (req.path === '/ping') return res.status(200).send({});
    res.reply = ({ code, message }, data = {}, header = undefined) => {
        res.status(code)
            .header(header)
            .json({ message, data });
    };
    next();
};

Router.prototype.routeHandler = function(req, res) {
    res.status(404);
    res.send({ message: 'Route not found' });
};

Router.prototype.logErrors = function(err, req, res, next) {
    log.error(`${req.method} ${req.url}`);
    log.error('body -> ', req.body);
    log.error(err.stack);
    return next(err);
};

Router.prototype.errorHandler = function(err, req, res, next) {
    res.status(500);
    res.send({ message: err });
};

module.exports = new Router();
