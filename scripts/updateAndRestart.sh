#!/bin/bash

# any future command that fails will exit the script
set -e

# source the nvm file. In an non
# If you are not using nvm, add the actual path like
source /home/ubuntu/.nvm/nvm.sh

# Delete the old repo
rm -rf /home/ubuntu/projects/boilerplate
mkdir -p /home/ubuntu/projects

cd /home/ubuntu/projects

git clone -b $1 git@gitlab.com:boilerplate/boilerplate.git
# clone the repo again

cd /home/ubuntu/projects/boilerplate

# install npm packages
echo "Running npm install"
npm install


# stop the previous pm2
{
    pm2 delete boilerplate
} || { 
    pm2 kill
}

pm2 status
NODE_ENV=$1 HOST=$2 pm2 start index.js -n boilerplate