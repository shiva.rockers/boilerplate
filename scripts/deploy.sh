#!/bin/bash

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
# deploy/\ disableHostKeyChecking.sh

set -e
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
if [ $CI_ENVIRONMENT_NAME == 'prod' ]
then
  DEPLOY_SERVERS=${PRODUCTION_IP}
else 
  DEPLOY_SERVERS=${STAGGING_IP}
fi


# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

# Lets iterate over this array and ssh into each EC2 instance
# Once inside.
# 1. Stop the server
# 2. Take a pull
# 3. Start the server
echo "Starting deployment in ${CI_COMMIT_BRANCH} branch with commit ${CI_COMMIT_TITLE}"

curl -X POST --data-urlencode "payload={\"channel\": \"#builds\",\"text\": \"Starting deployment in *${CI_COMMIT_BRANCH}* branch with commit *${CI_COMMIT_TITLE}*\"}" https://hooks.slack.com/services/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


for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  ssh ubuntu@${server} 'bash -s' < ./deploy/updateAndRestart.sh ${CI_COMMIT_BRANCH} ${server}
done
