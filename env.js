/* eslint-disable no-var */
/* eslint-disable no-use-before-define */
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
process.env.HOST = process.env.HOST || '127.0.0.1';
process.env.PORT = 8080;

const oEnv = {};

oEnv.dev = {
    BASE_URL: `http://127.0.0.1:3000`,
    DB_URL: '',
};

oEnv.stag = {
    BASE_URL: `http://127.0.0.1:3000`,
    DB_URL: '',
};

oEnv.prod = {
    BASE_URL: `http://127.0.0.1:3000`,
    DB_URL: '',
};

process.env.BASE_URL = oEnv[process.env.NODE_ENV].BASE_URL;
process.env.DB_URL = oEnv[process.env.NODE_ENV].DB_URL;
process.env.JWT_SECRET = 'jwt-secret';
process.env.OTP_VALIDITY = 60 * 1000;

console.log(process.env.NODE_ENV, process.env.HOST, 'configured');
